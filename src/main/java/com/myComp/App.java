package com.myComp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;
import java.util.Properties;


public class App {
    private static final String PROP_FILE_NAME = "app.properties";
    private static final String INCREMENT = "increment";
    private static final String FIRST_NUMBER = "number.first";
    private static final String SECOND_NUMBER = "number.second";
    public static final Logger logger = LoggerFactory.getLogger(App.class);
    private static final String WARN_INC_IS_0 = "Increment shouldn't be 0";
    private static final String WARN_SECOND_BIGGER_FIRST = "First value mustn't be bigger than second";
    private static final String NO_RESULTS = "No results";

    public static void main(String[] args) {
        String type = System.getProperty("type");
        if (type == null) type = "int";
        Properties properties = getProperties();
        if (!properties.containsKey(INCREMENT) || !properties.containsKey(FIRST_NUMBER)
                || !properties.containsKey(SECOND_NUMBER)) {
            logger.warn("'app.properties' file is not correct");
            return;
        }
        double incDouble = 0;
        double firstDouble = 0;
        double secondDouble = 0;
        try {
            incDouble = Double.parseDouble(properties.getProperty(INCREMENT));
            firstDouble = Double.parseDouble(properties.getProperty(FIRST_NUMBER));
            secondDouble = Double.parseDouble(properties.getProperty(SECOND_NUMBER));
            if (incDouble < 0) {
                logger.warn("Increment must be bigger than 0");
                return;
            }
        } catch (NumberFormatException e) {
            logger.warn(e.getMessage());
            logger.warn("Don't looks like a number");
        }
        App app = new App();
        String message = "";
        switch (type.toLowerCase(Locale.ROOT)) {
            case "int":
                message = app.printIntCalculation(incDouble, firstDouble, secondDouble);
                break;
            case "byte":
                message = app.printByteCalculation(incDouble, firstDouble, secondDouble);
                break;
            case "short":
                message = app.printShortCalculation(incDouble, firstDouble, secondDouble);
                break;
            case "long":
                message = app.printLongCalculation(incDouble, firstDouble, secondDouble);
                break;
            case "float":
                message = app.printFloatCalculation(incDouble, firstDouble, secondDouble);
                break;
            case "double":
                message = app.printDoubleCalculation(incDouble, firstDouble, secondDouble);
                break;
            default:
                logger.warn("Type is not required");
                break;
        }
        logger.info(message);
    }

    protected String printDoubleCalculation(double increment, double firstDouble, double secondNumbere) {
        MathContext round3SigFig = new MathContext(6, RoundingMode.HALF_UP);
        BigDecimal inc = BigDecimal.valueOf(increment);
        BigDecimal first = BigDecimal.valueOf(firstDouble);
        BigDecimal second = BigDecimal.valueOf(secondNumbere);
        if (inc.compareTo(BigDecimal.valueOf(0.1)) < 0 && inc.compareTo(BigDecimal.valueOf(-0.1)) > 0) {
            logger.warn("Increment is to small");
            return NO_RESULTS;
        }
        if (first.compareTo(second) > 0) {
            logger.warn(WARN_SECOND_BIGGER_FIRST);
            return NO_RESULTS;
        }

        ArrayList<BigDecimal> list = new ArrayList<>();
        while (first.compareTo(second) < 0 && Math.abs(first.doubleValue()) >= firstDouble) {
            list.add(first);
            first = BigDecimal.valueOf(first.doubleValue() + inc.doubleValue()).round(round3SigFig);
        }
        StringBuilder sb = new StringBuilder();
        list.forEach(x -> {
            list.forEach(y -> sb.append(x).append(" * ").append(y).append(" = ")
                    .append(BigDecimal.valueOf(x.doubleValue() * y.doubleValue()).round(round3SigFig)).append("\n"));
            sb.append("\n");
        });
        return sb.toString();
    }

    protected String printFloatCalculation(Double incDouble, Double firstDouble, Double secondDouble) {
        float inc = incDouble.floatValue();
        float first = firstDouble.floatValue();
        float second = secondDouble.floatValue();
        if (inc > -0.1 && inc < 0.1) {
            logger.warn("Increment is to small");
            return NO_RESULTS;
        }
        if (first > second) {
            logger.warn(WARN_SECOND_BIGGER_FIRST);
            return NO_RESULTS;
        }

        ArrayList<Float> list = new ArrayList<>();
        while (first < second) {
            list.add(first);
            if (first + inc  < Byte.MAX_VALUE) {
                first += inc;
            } else break;
        }
        StringBuilder sb = new StringBuilder();
        list.forEach(x -> {
            list.forEach(y -> sb.append(x).append(" * ").append(y).append(" = ").append(x * y).append("\n"));
            sb.append("\n");
        });
        return sb.toString();
    }

    protected String printLongCalculation(Double incDouble, Double firstDouble, Double secondDouble) {
        long inc = incDouble.longValue();
        long first = firstDouble.longValue();
        long second = secondDouble.longValue();
        if (inc == 0) {
            logger.warn(WARN_INC_IS_0);
            return NO_RESULTS;
        }
        if (first > second) {
            logger.warn(WARN_SECOND_BIGGER_FIRST);
            return NO_RESULTS;
        }

        ArrayList<Long> list = new ArrayList<>();
        while (first < second) {
            list.add(first);
            if (first + inc < Byte.MAX_VALUE) {
                first += inc;
            } else break;
        }
        StringBuilder sb = new StringBuilder();
        list.forEach(x -> {
            list.forEach(y -> sb.append(x).append(" * ").append(y).append(" = ").append(x * y).append("\n"));
            sb.append("\n");
        });
        return sb.toString();
    }

    protected String printShortCalculation(Double incDouble, Double firstDouble, Double secondDouble) {
        short inc = incDouble.shortValue();
        short first = firstDouble.shortValue();
        short second = secondDouble.shortValue();
        if (inc == 0) {
            logger.warn(WARN_INC_IS_0);
            return NO_RESULTS;
        }
        if (first > second) {
            logger.warn(WARN_SECOND_BIGGER_FIRST);
            return NO_RESULTS;
        }

        ArrayList<Short> list = new ArrayList<>();
        while (first < second) {
            list.add(first);
            if (first + inc < Byte.MAX_VALUE) {
                first += inc;
            } else break;
        }
        StringBuilder sb = new StringBuilder();
        list.forEach(x -> {
            list.forEach(y -> sb.append(x).append(" * ").append(y).append(" = ").append(x * y).append("\n"));
            sb.append("\n");
        });
        return sb.toString();
    }

    protected String printByteCalculation(Double incDouble, Double firstDouble, Double secondDouble) {
        byte inc = incDouble.byteValue();
        byte first = firstDouble.byteValue();
        byte second = secondDouble.byteValue();
        if (inc == 0) {
            logger.warn(WARN_INC_IS_0);
            return NO_RESULTS;
        }
        if (first > second) {
            logger.warn(WARN_SECOND_BIGGER_FIRST);
            return NO_RESULTS;
        }
        ArrayList<Byte> list = new ArrayList<>();
        while (first < second) {
            list.add(first);
            if (first + inc < Byte.MAX_VALUE) {
                first += inc;
            } else break;
        }
        StringBuilder sb = new StringBuilder();
        list.forEach(x -> {
            list.forEach(y -> sb.append(x).append(" * ").append(y).append(" = ").append(x * y).append("\n"));
            sb.append("\n");
        });
        return sb.toString();

    }

    protected String printIntCalculation(Double incDouble, Double firstDouble, Double secondDouble) {
        int inc = incDouble.intValue();
        int first = firstDouble.intValue();
        int second = secondDouble.intValue();
        if (inc == 0) {
            logger.warn(WARN_INC_IS_0);
            return NO_RESULTS;
        }
        if (first > second) {
            logger.warn(WARN_INC_IS_0);
            return NO_RESULTS;
        }
        ArrayList<Integer> list = new ArrayList<>();
        while (first < second) {
            list.add(first);
            if (first + inc < Integer.MAX_VALUE) {
                first += inc;
            } else break;
        }
        StringBuilder sb = new StringBuilder();
        list.forEach(x -> {
            list.forEach(y -> sb.append(x).append(" * ").append(y).append(" = ").append(x * y).append("\n"));
            sb.append("\n");
        });
        return sb.toString();

    }


    protected static Properties getProperties() {
        System.setProperty("file.encoding", "UTF-8");
        Properties prop = new Properties();
        File file = new File("./" + PROP_FILE_NAME);
        if (file.exists()) {
            try (InputStreamReader in = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8)) {
                prop.load(in);
            } catch (IOException e) {
                logger.warn("No 'app.property' file", e);
            }
        } else {
            try {
                prop.load(new InputStreamReader(Objects.requireNonNull(App.class.getClassLoader().
                        getResourceAsStream(PROP_FILE_NAME)), StandardCharsets.UTF_8));
            } catch (IOException e) {
                logger.warn("No 'app.properties' near jar. So using default file", e);
            }
        }
        return prop;
    }
}

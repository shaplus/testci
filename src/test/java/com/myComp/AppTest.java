package com.myComp;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for simple App.
 */


class AppTest {

    private static final String WARN_INC_IS_0 = "Increment shouldn't be 0";
    private static final String WARN_SECOND_BIGGER_FIRST = "First value mustn't be bigger than second";
    private static final String NO_RESULTS = "No results";

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    @Test
    @Timeout(1000)
    void testInt() {
        App app = new App();
        assertEquals("1 * 1 = 1\n\n", app.printIntCalculation(10.0, 1.0, 10.0));
        assertEquals("1 * 1 = 1\n\n", app.printIntCalculation(10.0, 1.5, 10.0));
        assertEquals(NO_RESULTS, app.printIntCalculation(0.0, 1.0, 10.0));
        assertEquals(NO_RESULTS, app.printIntCalculation(1.0, 11.0, 10.0));

    }

    @Test
    @Timeout(1000)
    void testByte() {
        App app = new App();
        assertEquals("1 * 1 = 1\n\n", app.printByteCalculation(10.0, 1.0, 10.0));
        assertEquals("1 * 1 = 1\n\n", app.printByteCalculation(10.0, 1.5, 10.0));
        assertEquals(NO_RESULTS, app.printByteCalculation(0.0, 1.0, 10.0));
        assertEquals(NO_RESULTS, app.printByteCalculation(1.0, 11.0, 10.0));
        assertEquals("100 * 100 = 10000\n\n", app.printByteCalculation(100.0, 100.0, 120.0));
    }

    @Test
    @Timeout(1000)
    void testShort() {
        App app = new App();
        assertEquals("1 * 1 = 1\n\n", app.printShortCalculation(10.0, 1.0, 10.0));
        assertEquals("1 * 1 = 1\n\n", app.printShortCalculation(10.0, 1.5, 10.0));
        assertEquals(NO_RESULTS, app.printShortCalculation(0.0, 1.0, 10.0));
        assertEquals(NO_RESULTS, app.printShortCalculation(1.0, 11.0, 10.0));
        assertEquals("32766 * 32766 = "+ 32766*32766+"\n\n", app.printShortCalculation(10.0, 32766.0, 32767.0));

    }

    @Test
    @Timeout(1000)
    void testFloat() {
        App app = new App();
        assertEquals("1.0 * 1.0 = 1.0\n\n", app.printFloatCalculation(10.0, 1.0, 10.0));
        assertEquals("1.5 * 1.5 = 2.25\n\n", app.printFloatCalculation(10.0, 1.5, 10.0));
        assertEquals(NO_RESULTS, app.printFloatCalculation(0.0, 1.0, 10.0));
        assertEquals(NO_RESULTS, app.printFloatCalculation(1.0, 11.0, 10.0));

    }

    @Test
    @Timeout(1000)
    void testLong() {
        App app = new  App();
        assertEquals("1 * 1 = 1\n\n", app.printLongCalculation(10.0, 1.0, 10.0));
        assertEquals("1 * 1 = 1\n\n", app.printLongCalculation(10.0, 1.5, 10.0));
        assertEquals(NO_RESULTS, app.printLongCalculation(0.0, 1.0, 10.0));
        assertEquals(NO_RESULTS, app.printLongCalculation(1.0, 11.0, 10.0));

    }

    @Test
    @Timeout(1000)
    void testDouble() {
        App app = new App();
        assertEquals("1.0 * 1.0 = 1.0\n\n", app.printDoubleCalculation(10.0, 1.0, 10.0));
        assertEquals("1.5 * 1.5 = 2.25\n\n", app.printDoubleCalculation(10.0, 1.5, 10.0));
        assertEquals(NO_RESULTS, app.printDoubleCalculation(0.0, 1.0, 10.0));
        assertEquals(NO_RESULTS, app.printDoubleCalculation(1.0, 11.0, 10.0));

    }
}
